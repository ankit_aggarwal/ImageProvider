package com.example.ankit.imageprovider;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.UUID;

public class PhotoGalleryActivity extends AppCompatActivity {

    private ProgressDialog pdImages;

    private SharedPreferences mSharedPreferences;
    private File mImagesDir;

    // Array of files in the images subdirectory
    private File[] mImageFiles;

    //result intent
    private Intent mResultIntent;

    private static final String PREF_NAME = "IMAGE_PROVIDER";
    private static final String KEY_FILES_ADDED = "KEY_FILES_ADDED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pdImages = new ProgressDialog(this);
        pdImages.setMessage("Please wait...");
        pdImages.setCancelable(false);

        mSharedPreferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        mImagesDir = new File(getFilesDir(), "images");

        boolean result = mImagesDir.mkdir();
        if (result) {
            Log.i("MKDIR", "SUCCESS");
        }

        if (!mSharedPreferences.getBoolean(KEY_FILES_ADDED, false)) {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    pdImages.show();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    getAllImagesAndStoreThemLocally();
                    mSharedPreferences.edit().putBoolean(KEY_FILES_ADDED, true).apply();
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    updateUI();
                }
            }.execute();
        } else {
            updateUI();
        }
    }

    private void updateUI() {
        pdImages.dismiss();

        // Set up an Intent to send back to apps that request a file
        mResultIntent = new Intent("com.example.ankit.imageprovider.ACTION_RETURN_FILE");

        // Get the files in the images subdirectory
        mImageFiles = mImagesDir.listFiles();

        // Set the Activity's result to null to begin with
        setResult(Activity.RESULT_CANCELED, null);

        RecyclerView rvGalleryImages = (RecyclerView) findViewById(R.id.rv_gallery_images);

        /*StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        rvGalleryImages.setLayoutManager(staggeredGridLayoutManager);*/

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        rvGalleryImages.setLayoutManager(gridLayoutManager);

        ImagesAdapter imagesAdapter = new ImagesAdapter(this, mImageFiles);
        rvGalleryImages.setAdapter(imagesAdapter);
        imagesAdapter.notifyDataSetChanged();
    }

    private void getAllImagesAndStoreThemLocally() {
        Cursor imageCursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);

        if (imageCursor != null) {
            while (imageCursor.moveToNext()) {
                try {
                    File outputFile = new File(mImagesDir, constructCapturedPhotoName());
                    FileOutputStream destStream = new FileOutputStream(outputFile);

                    File inputFile = new File(imageCursor.getString(0));
                    FileInputStream srcStream = new FileInputStream(inputFile);
                    copyInputStreamToOutputStream(srcStream, destStream);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            imageCursor.close();
        }
    }

    private String constructCapturedPhotoName() {
        return (UUID.randomUUID().toString() + ".jpg");
    }

    private void copyInputStreamToOutputStream(FileInputStream inputStream, FileOutputStream outputStream) {
        try {
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, len);
            }
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //public function to handle onclick events in adapter
    public void selectImage(int position) {

        if (!Intent.ACTION_PICK.equals(getIntent().getAction())) {
            return;
        }

        File requestFile = mImageFiles[position];
                /*
                 * Most file-related method calls need to be in
                 * try-catch blocks.
                 */
        // Use the FileProvider to get a content URI
        try {
            Uri fileUri = FileProvider.getUriForFile(this, "com.example.ankit.imageprovider.fileprovider", requestFile);

            if (fileUri != null) {
                // Grant temporary read permission to the content URI
                mResultIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                // Put the Uri and MIME type in the result Intent
                mResultIntent.setDataAndType(fileUri, getContentResolver().getType(fileUri));
                // Set the result
                setResult(Activity.RESULT_OK, mResultIntent);
            } else {
                mResultIntent.setDataAndType(null, "");
                setResult(RESULT_CANCELED, mResultIntent);
            }

            finish();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
