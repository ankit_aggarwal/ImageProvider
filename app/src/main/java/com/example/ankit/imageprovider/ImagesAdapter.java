package com.example.ankit.imageprovider;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.PhotoViewHolder> {

    private File[] mImageFiles;
    private LayoutInflater mLayoutInflater;
    private Picasso picasso;

    public ImagesAdapter(Context context, File[] imageFiles) {
        this.mImageFiles = imageFiles;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.picasso = Picasso.with(context);
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoViewHolder(mLayoutInflater.inflate(R.layout.adapter_images, parent, false));
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        picasso.load(mImageFiles[position])
                .transform(new BitmapTransform(200, 200))
                .into(holder.ivPhoto);
    }

    @Override
    public int getItemCount() {
        return mImageFiles.length;
    }

    public static class PhotoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivPhoto;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ivPhoto = (ImageView) itemView.findViewById(R.id.iv_image);
            ivPhoto.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_image:
                    ((PhotoGalleryActivity) v.getContext()).selectImage(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }
}
